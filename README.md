# i3wm

Custom i3wm config on RaspberrypiOS

## Installation

```
$ sudo apt install i3
$ echo 'exec i3' > ~/.xsession
$ reboot
  
$ i3 --version
i3 version 4.16.1 (2019-01-27) © 2009 Michael Stapelberg and contributors
```

## Features

- `.config/i3/config`
- `.config/i3status/config`
- `.config/i3/scripts`

## i3wm default keybindings

- `$mod` refers to the modifier key (alt by default)

### General

- `startx i3`       start i3 from command line
- `$mod+<Enter>`    open a terminal
- `$mod+d`          open dmenu (text based program launcher)
- `$mod+r`          resize mode (<Esc> or <Enter> to leave resize mode)
- `$mod+shift+e`    exit i3
- `$mod+shift+r`    restart i3 in place
- `$mod+shift+c`    reload config file
- `$mod+shift+q`    kill window (does normal close if application supports it)

### Windows

- `$mod+w`          tabbed layout
- `$mod+e`          vertical and horizontal layout (switches to and between them)
- `$mod+s`          stacked layout
- `$mod+f`          fullscreen

### Moving Windows

- `$mod+shift+<direction key>` Move window in _direction_ (depends on direction keys settings)

## Misc

i3wm screenshot :
![i3wm screenshot](shot1.png)
