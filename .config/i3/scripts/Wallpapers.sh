#!/bin/bash

while true
do
  feh --randomize --bg-fill ${HOME}/Downloads/Wallpapers/*
  sleep 300
done
